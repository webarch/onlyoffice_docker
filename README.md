# ONLYOFFICE Docker Ansible Role

*This role doesn't fully work, see [Problems configuring Apache as a reverse
proxy to ONLYOFFICE in
Docker](https://github.com/ONLYOFFICE/onlyoffice-nextcloud/issues/248).*

The [ONLYOFFICE Document
Server](https://git.coop/webarch/onlyoffice-documentserver) Ansible role can be
used for **a stand alone ONLYOFFICE Document Server without Docker** (this is
in use in production and is being maintained), see the [Nextcloud
server](https://git.coop/webarch/nextcloud-server) repo which builds
development servers for Nextcloud and ONLYOFFICE Document Server.

---------------------------------------------------------------------------------

This Ansible Galaxy role was written to be imported into the [Nextcloud
role](https://git.coop/webarch/nextcloud-server) when the [ONLYOFFICE
app](https://github.com/ONLYOFFICE/onlyoffice-nextcloud) is set to be
installed.

This role is based on the [ONLYOFFICE Document Server Community Edition Docker
version](https://helpcenter.onlyoffice.com/server/docker/document/docker-installation.aspx)
being installed on the same server as Nextcloud and [Apache is being used as a
reverse
proxy](https://github.com/ONLYOFFICE/document-server-proxy/blob/master/apache/proxy-https-to-https.conf).

## Notes

To enter the ONLYOFFICE container:

```bash
docker exec -t -i $(docker ps -q) /bin/bash
```

This issue is to be solved, [Error while downloading the document file to be converted](https://github.com/ONLYOFFICE/onlyoffice-owncloud/issues/181).
